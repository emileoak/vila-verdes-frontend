import { NgModule } from '@angular/core';
import { UiComponent } from './ui.component';
import { BannerComponent } from './components/banner/banner.component';
import { ButtonModule } from 'primeng/button';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [UiComponent, BannerComponent],
  imports: [CommonModule, ButtonModule],
  exports: [UiComponent, BannerComponent],
})
export class UiModule {}
