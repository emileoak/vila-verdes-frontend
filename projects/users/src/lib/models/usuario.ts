export class Usuario {
  id?: number;
  nome?: string;
  sobrenome?: string;
  email?: string;
  senha?: string;
  endereco?: string;
  celular?: string;
  cpf?: string;
  googleId?: string;
  facebookId?: string;
  tipo?: number;
  active?: boolean;
  createdAt?: string;
  updatedAt?: string;
  token?: string;
}
