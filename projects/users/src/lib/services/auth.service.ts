import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'env/environment.prod';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario';
import { LocalstorageService } from './localstorage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  apiUrlUsuario = environment.apiUrl + 'usuarios';

  constructor(
    private http: HttpClient,
    private token: LocalstorageService,
    private router: Router
  ) {}

  login(email: string, senha: string): Observable<any> {
    return this.http.post<Usuario>(`${this.apiUrlUsuario}/login`, {
      email,
      senha,
    });
  }

  logout() {
    this.token.removeToken();
    this.router.navigate(['/login']);
  }
}
