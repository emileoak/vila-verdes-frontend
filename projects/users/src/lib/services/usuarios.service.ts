import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'env/environment';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root',
})
export class UsuariosService {
  apiUrlUsuario = environment.apiUrl + 'usuarios';
  constructor(private http: HttpClient) {}

  getUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.apiUrlUsuario);
  }

  getUsuario(usuarioId: number): Observable<Usuario> {
    return this.http.get<Usuario>(`${this.apiUrlUsuario}/${usuarioId}`);
  }

  createUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.apiUrlUsuario, usuario);
  }

  updateUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(
      `${this.apiUrlUsuario}/${usuario.id}`,
      usuario
    );
  }

  deleteUsuario(usuarioId: number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrlUsuario}/${usuarioId}`);
  }
}
