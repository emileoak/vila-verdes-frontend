import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { LocalstorageService } from '../../services/localstorage.service';

@Component({
  selector: 'usuarios-login',
  templateUrl: './login.component.html',
  styles: [],
})
export class LoginComponent implements OnInit {
  loginFormGroup: FormGroup;
  isSubmitted = false;
  authError = false;
  authMessage = 'Email ou Senha inválidos';

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private localstorageService: LocalstorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._initLoginForm();
  }

  private _initLoginForm() {
    this.loginFormGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      senha: ['', Validators.required],
    });
  }

  onSubmit() {
    this.isSubmitted = true;

    if (this.loginFormGroup.invalid) return;

    this.auth.login(this.loginForm.email, this.loginForm.senha).subscribe(
      (res) => {
        this.authError = false;
        this.localstorageService.setToken(res.token);
        console.log("usuario: ", res)
        if (res.usuario.tipo === 2) {
          this.router.navigate(['/']);
        } else {
          //todo alterar
          window.open('http://dev.vilaverdes.com.br/loja/', '_self');
        }
      },
      (error: HttpErrorResponse) => {
        this.authError = true;
        if (error.status !== 400) {
          this.authMessage =
            'Erro No servidor. Por favor, tente novamente mais tarde!';
        }
      }
    );
  }

  get loginForm() {
    return this.loginFormGroup.value;
  }
}
