import { Component, OnInit } from '@angular/core';
import { AuthService } from 'projects/users/src/public-api';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit(): void {}

  logoutUser() {
    this.authService.logout();
  }
  goToSite() {
    window.open('http://dev.vilaverdes.com.br/loja/');
  }
}
