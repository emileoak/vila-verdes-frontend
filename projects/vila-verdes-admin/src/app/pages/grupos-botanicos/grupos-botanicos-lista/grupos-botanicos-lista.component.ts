import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import {
  GrupoBotanico,
  GruposBotanicosService,
} from 'projects/products/src/public-api';

@Component({
  selector: 'app-grupos-botanicos-lista',
  templateUrl: './grupos-botanicos-lista.component.html',
  styles: [],
})
export class GruposBotanicosListaComponent implements OnInit {
  gruposBotanicos: GrupoBotanico[] = [];
  constructor(
    private confirmationService: ConfirmationService,
    private gruposBotanicosService: GruposBotanicosService,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._getGruposBotanicos();
  }

  private _getGruposBotanicos() {
    this.gruposBotanicosService.getGruposBotanicos().subscribe((grupos) => {
      this.gruposBotanicos = grupos;
    });
  }
  deleteGrupoBotanico(grupoBotanicoId: number) {
    this.confirmationService.confirm({
      message: 'Você quer deletar este grupo botânico?',
      header: 'Deletar Grupo Botânico',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.gruposBotanicosService
          .deleteGrupoBotanico(grupoBotanicoId)
          .subscribe(
            () => {
              this._getGruposBotanicos();
              this.messageService.add({
                severity: 'success',
                summary: 'Sucesso',
                detail: 'Grupo botânico removido',
              });
            },
            () => {
              this.messageService.add({
                severity: 'error',
                summary: 'Erro',
                detail: 'Grupo botânico não removido',
              });
            }
          );
      }
    });
  }

  updateGrupobotanico(grupoBotanicoId: number) {
    this.router.navigateByUrl(`grupos-botanicos/form/${grupoBotanicoId}`);
  }
}
