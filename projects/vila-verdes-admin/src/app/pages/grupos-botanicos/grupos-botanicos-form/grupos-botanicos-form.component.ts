import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import {
  GrupoBotanico,
  GruposBotanicosService,
} from 'projects/products/src/public-api';
import { timer } from 'rxjs';

@Component({
  selector: 'app-grupos-botanicos-form',
  templateUrl: './grupos-botanicos-form.component.html',
  styles: [],
})
export class GruposBotanicosFormComponent implements OnInit {
  form: FormGroup;
  isSubmitted = false;
  editmode = false;
  currentGrupoBotanicoId: number;

  constructor(
    private formBuilder: FormBuilder,
    private gruposBotanicosService: GruposBotanicosService,
    public location: Location,
    private messageService: MessageService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nome: ['', Validators.required],
      descricao: ['', Validators.required],
    });

    this._checkEditMode();
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) {
      return;
    }
    const grupoBotanico: GrupoBotanico = {
      id: this.currentGrupoBotanicoId,
      nome: this.grupoBotanicoForm.nome,
      descricao: this.grupoBotanicoForm.descricao,
    };
    if (this.editmode) {
      this._updateGrupoBotanico(grupoBotanico);
    } else {
      this._addGrupoBotanico(grupoBotanico);
    }
  }

  private _addGrupoBotanico(grupoBotanico: GrupoBotanico) {
    this.gruposBotanicosService.createGrupoBotanico(grupoBotanico).subscribe(
      (grupoBotanico: GrupoBotanico) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: `Grupo botânico ${grupoBotanico.nome} foi criado`,
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Grupo botânico não foi criado',
        });
      }
    );
  }

  private _updateGrupoBotanico(grupoBotanico: GrupoBotanico) {
    this.gruposBotanicosService.updateGrupoBotanico(grupoBotanico).subscribe(
      () => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Grupo botânico alterado.',
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Grupo botânico não foi criado',
        });
      }
    );
  }

  private _checkEditMode() {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.editmode = true;
        this.currentGrupoBotanicoId = params['id'];
        this.gruposBotanicosService
          .getGrupoBotanico(params['id'])
          .subscribe((grupoBotanico) => {
            this.form.patchValue({
              nome: grupoBotanico.nome,
              descricao: grupoBotanico.descricao,
            });
          });
      }
    });
  }

  get grupoBotanicoForm() {
    return this.form.value;
  }
}
