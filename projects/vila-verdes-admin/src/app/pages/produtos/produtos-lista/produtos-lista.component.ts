import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ProdutosService } from 'projects/products/src/public-api';

@Component({
  selector: 'products-produtos-lista',
  templateUrl: './produtos-lista.component.html',
  styles: [],
})
export class ProdutosListaComponent implements OnInit {
  produtos = [];
  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private produtosService: ProdutosService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._getProdutos();
  }

  private _getProdutos() {
    this.produtosService.getProdutos().subscribe((produtos) => {
      this.produtos = produtos;
    });
  }

  deleteProduto(produtoId: number) {
    this.confirmationService.confirm({
      message: 'Você quer deletar este produto?',
      header: 'Deletar Produto',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.produtosService.deleteProduto(produtoId).subscribe(
          () => {
            this._getProdutos();
            this.messageService.add({
              severity: 'success',
              summary: 'Sucesso',
              detail: 'Produto removido',
            });
          },
          () => {
            this.messageService.add({
              severity: 'error',
              summary: 'Erro',
              detail: 'Produto não removido',
            });
          }
        );
      },
    });
  }

  updateProduto(produtoId: number) {
    this.router.navigateByUrl(`produtos/form/${produtoId}`);
  }
}
