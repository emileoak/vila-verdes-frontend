import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import {
  Planta,
  PlantasService,
  Produto,
  ProdutosService,
} from 'projects/products/src/public-api';
import { timer } from 'rxjs';

@Component({
  selector: 'app-produtos-form',
  templateUrl: './produtos-form.component.html',
  styles: [],
})
export class ProdutosFormComponent implements OnInit {
  editmode = false;
  form: FormGroup;
  isSubmitted = false;
  currentProdutoId: number;
  plantas = [];

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public location: Location,
    private messageService: MessageService,
    private plantasService: PlantasService,
    private produtossService: ProdutosService
  ) {}

  ngOnInit(): void {
    this._initForm();
    this._checkEditMode();
    this._getPlantas();
  }
  private _initForm() {
    this.form = this.formBuilder.group({
      nome: ['', Validators.required],
      descricao: [''],
      planta: [''],
      valor: ['', Validators.required],
      desconto: [''],
      quantidade: ['', Validators.required],
      active: [''],
    });
  }

  private _getPlantas() {
    this.plantasService.getPlantas().subscribe((plantas) => {
      this.plantas = plantas || [];
    });
  }

  onSubmit() {
    console.log('tentanod salvar');
    this.isSubmitted = true;
    console.log('produtoForm: ', this.produtoForm);
    if (this.form.invalid) {
      console.log('invalid: ');
      return;
    }
    const produto: Produto = {
      id: this.currentProdutoId,
      nome: this.produtoForm.nome,
      descricao: this.produtoForm.descricao,
      plantaId: this.produtoForm.planta?.id,
      valor: this.produtoForm.valor,
      desconto: this.produtoForm.desconto == '' ? 0 : this.produtoForm.desconto,
      quantidade: this.produtoForm.quantidade,
      active: this.produtoForm.active,
    };
    if (this.editmode) {
      this._updateProduto(produto);
    } else {
      this._addProduto(produto);
    }
  }

  private _addProduto(produto: Produto) {
    console.log('produto: ', produto);
    this.produtossService.createProduto(produto).subscribe(
      (produto: Produto) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: `Produto ${produto.nome} foi criado`,
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Produto não foi criado',
        });
      }
    );
  }

  private _updateProduto(produto: Produto) {
    this.produtossService.updateProduto(produto).subscribe(
      () => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Produto alterado.',
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Produto não foi alterado',
        });
      }
    );
  }

  private _checkEditMode() {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.editmode = true;
        this.currentProdutoId = params['id'];
        this.produtossService.getProduto(params['id']).subscribe((produto) => {
          this.form.patchValue({
            nome: produto.nome,
            descricao: produto.descricao,
            planta: produto.planta,
            valor: produto.valor,
            desconto: produto.desconto,
            quantidade: produto.quantidade,
            active: produto.active,
          });
        });
      }
    });
  }

  get produtoForm() {
    return this.form.value;
  }
}
