import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { PlantasService } from 'projects/products/src/public-api';

@Component({
  selector: 'app-plantas-lista',
  templateUrl: './plantas-lista.component.html',
  styles: [],
})
export class PlantasListaComponent implements OnInit {
  plantas = [];
  
  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private plantasService: PlantasService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._getPlantas();
  }

  private _getPlantas() {
    this.plantasService.getPlantas().subscribe((plantas) => {
      this.plantas = plantas;
    });
  }

  deletePlanta(plantaId: number) {
    this.confirmationService.confirm({
      message: 'Você quer deletar esta planta?',
      header: 'Deletar Planta',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.plantasService.deletePlanta(plantaId).subscribe(
          () => {
            this._getPlantas();
            this.messageService.add({
              severity: 'success',
              summary: 'Sucesso',
              detail: 'Planta removida',
            });
          },
          () => {
            this.messageService.add({
              severity: 'error',
              summary: 'Erro',
              detail: 'Planta não removida',
            });
          }
        );
      },
    });
  }

  updatePlanta(plantaId: number) {
    this.router.navigateByUrl(`plantas/form/${plantaId}`);
  }
}
