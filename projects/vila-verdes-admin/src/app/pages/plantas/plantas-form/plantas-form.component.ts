import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import {
  GruposBotanicosService,
  Planta,
  PlantasService,
} from 'projects/products/src/public-api';
import { timer } from 'rxjs';

@Component({
  selector: 'app-plantas-form',
  templateUrl: './plantas-form.component.html',
  styles: [],
})
export class PlantasFormComponent implements OnInit {
  editmode = false;
  form: FormGroup;
  isSubmitted = false;
  currentPlantaId: number;
  gruposBotanicos = [];

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private gruposBotanicosService: GruposBotanicosService,
    public location: Location,
    private messageService: MessageService,
    private plantasService: PlantasService
  ) {}

  ngOnInit(): void {
    this._initForm();
    this._checkEditMode();
    this._getGruposBotanicos();
  }
  private _initForm() {
    this.form = this.formBuilder.group({
      nome: ['', Validators.required],
      especificacoes: [''],
      grupoBotanico: ['', Validators.required],
      descricao: [''],
      imagens: [''],
    });
  }

  private _getGruposBotanicos() {
    this.gruposBotanicosService
      .getGruposBotanicos()
      .subscribe((gruposBotanicos) => {
        this.gruposBotanicos = gruposBotanicos || [];
      });
  }

  onSubmit() {
    console.log('tentanod salvar');
    this.isSubmitted = true;
    console.log('plantaForm: ', this.plantaForm);
    if (this.form.invalid) {
      console.log('invalid: ');
      return;
    }
    const planta: Planta = {
      id: this.currentPlantaId,
      nome: this.plantaForm.nome,
      descricao: this.plantaForm.descricao,
      especificacoes: this.plantaForm.especificacoes,
      grupoBotanicoId: this.plantaForm.grupoBotanico?.id,
      imagens: [this.plantaForm.imagens],
    };
    if (this.editmode) {
      this._updatePlanta(planta);
    } else {
      this._addPlanta(planta);
    }
  }

  private _addPlanta(planta: Planta) {
    this.plantasService.createPlanta(planta).subscribe(
      (planta: Planta) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: `Planta ${planta.nome} foi criada`,
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Planta não foi criado',
        });
      }
    );
  }

  private _updatePlanta(planta: Planta) {
    this.plantasService.updatePlanta(planta).subscribe(
      () => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Planta alterado.',
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Planta não foi alterada',
        });
      }
    );
  }

  private _checkEditMode() {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.editmode = true;
        this.currentPlantaId = params['id'];
        this.plantasService.getPlanta(params['id']).subscribe((planta) => {
          this.form.patchValue({
            nome: planta.nome,
            descricao: planta.descricao,
            especificacoes: planta.especificacoes,
            grupoBotanico: planta.grupoBotanico,
            imagens: planta.imagens,
          });
        });
      }
    });
  }

  get plantaForm() {
    return this.form.value;
  }
}
