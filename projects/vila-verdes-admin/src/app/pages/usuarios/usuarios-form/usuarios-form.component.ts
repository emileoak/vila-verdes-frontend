import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Usuario, UsuariosService } from 'projects/users/src/public-api';
import { timer } from 'rxjs';

@Component({
  selector: 'app-usuarios-form',
  templateUrl: './usuarios-form.component.html',
  styles: [],
})
export class UsuariosFormComponent implements OnInit {
  editmode = false;
  form: FormGroup;
  isSubmitted = false;
  currentUsuarioId: number;
  userTypes = [{ n: 1 }, { n: 2 }];

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public location: Location,
    private messageService: MessageService,
    private usuarioService: UsuariosService
  ) {}

  ngOnInit(): void {
    this._initForm();
    this._checkEditMode();
  }
  private _initForm() {
    this.form = this.formBuilder.group({
      nome: ['', Validators.required],
      sobrenome: [''],
      email: ['', [Validators.required, Validators.email]],
      senha: ['', Validators.required],
      endereco: [''],
      celular: [''],
      cpf: [''],
     //googleId: [''],
      //facebookId: [''],
      tipo: [''],
      active: [''],
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) {
      return;
    }
    const usuario: Usuario = {
      id: this.currentUsuarioId,
      nome: this.usuarioForm.nome,
      sobrenome: this.usuarioForm.sobrenome,
      email: this.usuarioForm.email,
      senha: this.usuarioForm.senha,
      endereco: this.usuarioForm.endereco,
      celular: this.usuarioForm.celular,
      cpf: this.usuarioForm.cpf,
      googleId: this.usuarioForm.googleId,
      facebookId: this.usuarioForm.facebookId,
      tipo: this.usuarioForm.tipo.n,
      active: this.usuarioForm.active,
    };
    if (this.editmode) {
      this._updateUsuario(usuario);
    } else {
      this._addUsuario(usuario);
    }
  }

  private _addUsuario(usuario: Usuario) {
    this.usuarioService.createUsuario(usuario).subscribe(
      (usuario: Usuario) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: `Usuário ${usuario.nome} foi criado`,
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Usuário não foi criado',
        });
      }
    );
  }

  private _updateUsuario(usuario: Usuario) {
    this.usuarioService.updateUsuario(usuario).subscribe(
      () => {
        this.messageService.add({
          severity: 'success',
          summary: 'Sucesso',
          detail: 'Usuário alterado.',
        });
        timer(2000)
          .toPromise()
          .then(() => {
            this.location.back();
          });
      },
      () => {
        this.messageService.add({
          severity: 'error',
          summary: 'Erro',
          detail: 'Usuário não foi alterado',
        });
      }
    );
  }

  private _checkEditMode() {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.editmode = true;
        this.currentUsuarioId = params['id'];
        this.usuarioService.getUsuario(params['id']).subscribe((usuario) => {
          this.form.patchValue({
            nome: usuario.nome,
            sobrenome: usuario.sobrenome,
            email: usuario.email,
            senha: usuario.senha,
            endereco: usuario.endereco,
            celular: usuario.celular,
            cpf: usuario.cpf,
            googleId: usuario.googleId,
            facebookId: usuario.facebookId,
            tipo: usuario.tipo,
            active: usuario.active,
          });
        });
      }
    });
  }

  get usuarioForm() {
    return this.form.value;
  }
}
