import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { UsuariosService } from 'projects/users/src/public-api';

@Component({
  selector: 'app-usuarios-lista',
  templateUrl: './usuarios-lista.component.html',
  styles: [],
})
export class UsuariosListaComponent implements OnInit {
  usuarios = [];
  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private usuariosService: UsuariosService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._getUsuarios();
  }

  private _getUsuarios() {
    this.usuariosService.getUsuarios().subscribe((usuarios) => {
      this.usuarios = usuarios;
    });
  }

  deleteUsuario(usuarioId: number) {
    this.confirmationService.confirm({
      message: 'Você quer deletar este usuario?',
      header: 'Deletar Usuario',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.usuariosService.deleteUsuario(usuarioId).subscribe(
          () => {
            this._getUsuarios();
            this.messageService.add({
              severity: 'success',
              summary: 'Sucesso',
              detail: 'Usuario removido',
            });
          },
          () => {
            this.messageService.add({
              severity: 'error',
              summary: 'Erro',
              detail: 'Usuário não removido',
            });
          }
        );
      },
    });
  }

  updateUsuario(usuarioId: number) {
    this.router.navigateByUrl(`usuarios/form/${usuarioId}`);
  }
}
