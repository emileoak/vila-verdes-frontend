import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ShellComponent } from './shared/shell/shell.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { GruposBotanicosListaComponent } from './pages/grupos-botanicos/grupos-botanicos-lista/grupos-botanicos-lista.component';
import { ProdutosListaComponent } from './pages/produtos/produtos-lista/produtos-lista.component';
import { PlantasListaComponent } from './pages/plantas/plantas-lista/plantas-lista.component';
import { ProdutosFormComponent } from './pages/produtos/produtos-form/produtos-form.component';
import { PlantasFormComponent } from './pages/plantas/plantas-form/plantas-form.component';
import { UsuariosListaComponent } from './pages/usuarios/usuarios-lista/usuarios-lista.component';
import { UsuariosFormComponent } from './pages/usuarios/usuarios-form/usuarios-form.component';
import { GruposBotanicosService } from 'projects/products/src/public-api';
import {
  AuthGuard,
  JwtInterceptor,
  UsersModule,
} from 'projects/users/src/public-api';

import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { GruposBotanicosFormComponent } from './pages/grupos-botanicos/grupos-botanicos-form/grupos-botanicos-form.component';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DropdownModule } from 'primeng/dropdown';
import { InputMaskModule } from 'primeng/inputmask';
import { PedidosListaComponent } from './pages/pedidos/pedidos-lista/pedidos-lista.component';
import { AcessoriosListaComponent } from './pages/acessorios/acessorios-lista/acessorios-lista.component';
import { TiposAcessoriosListaComponent } from './pages/tipos-acessorios/tipos-acessorios-lista/tipos-acessorios-lista.component';
import { FilaListaComponent } from './pages/fila/fila-lista/fila-lista.component';

const UX_MODULE = [
  ButtonModule,
  CardModule,
  ConfirmDialogModule,
  DropdownModule,
  InputMaskModule,
  InputNumberModule,
  InputSwitchModule,
  InputTextareaModule,
  InputTextModule,
  TableModule,
  ToolbarModule,
  ToastModule,
];
const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: 'grupos-botanicos',
        component: GruposBotanicosListaComponent,
      },
      {
        path: 'grupos-botanicos/form',
        component: GruposBotanicosFormComponent,
      },
      {
        path: 'grupos-botanicos/form/:id',
        component: GruposBotanicosFormComponent,
      },
      {
        path: 'plantas',
        component: PlantasListaComponent,
      },
      {
        path: 'plantas/form',
        component: PlantasFormComponent,
      },
      {
        path: 'plantas/form/:id',
        component: PlantasFormComponent,
      },
      {
        path: 'produtos',
        component: ProdutosListaComponent,
      },
      {
        path: 'produtos/form',
        component: ProdutosFormComponent,
      },
      {
        path: 'produtos/form/:id',
        component: ProdutosFormComponent,
      },
      {
        path: 'usuarios',
        component: UsuariosListaComponent,
      },
      {
        path: 'usuarios/form',
        component: UsuariosFormComponent,
      },
      {
        path: 'usuarios/form/:id',
        component: UsuariosFormComponent,
      },
      {
        path: 'pedidos',
        component: PedidosListaComponent,
      },
      {
        path: 'acessorios',
        component: AcessoriosListaComponent,
      },
      {
        path: 'tipos-acessorios',
        component: TiposAcessoriosListaComponent,
      },
      {
        path: 'fila',
        component: FilaListaComponent,
      },
    ],
  },
];
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ShellComponent,
    SidebarComponent,
    GruposBotanicosListaComponent,
    GruposBotanicosFormComponent,
    ProdutosListaComponent,
    PlantasListaComponent,
    ProdutosFormComponent,
    PlantasFormComponent,
    UsuariosListaComponent,
    UsuariosFormComponent,
    PedidosListaComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes, { initialNavigation: 'enabled' }),
    UsersModule,
    ...UX_MODULE,
  ],
  providers: [
    ConfirmationService,
    GruposBotanicosService,
    MessageService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
