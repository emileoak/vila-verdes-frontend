import { GrupoBotanico } from './grupo-botanico';

export class Planta {
  id?: number;
  nome?: string;
  descricao?: string;
  especificacoes?: string;
  grupoBotanico?: GrupoBotanico;
  grupoBotanicoId?: number;
  imagens?: string[];
  createdAt?: string;
  updatedAt?: string;
}
