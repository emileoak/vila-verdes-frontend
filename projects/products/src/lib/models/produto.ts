import { Planta } from './planta';

export class Produto {
  id?: number;
  nome?: string;
  descricao?: string;
  planta?: Planta;
  plantaId?: number;
  acessorio?: string;
  valor?: number;
  desconto?: number;
  quantidade?: number;
  active?: boolean;
  createdAt?: string;
  updatedAt?: string;
}