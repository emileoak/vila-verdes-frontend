export class GrupoBotanico {
  id?: number;
  nome?: string;
  descricao?: string;
  createdAt?: string;
  updatedAt?: string;
}
