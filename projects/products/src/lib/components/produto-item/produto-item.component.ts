import { Component, Input, OnInit } from '@angular/core';
import { Produto } from '../../models/produto';

@Component({
  selector: 'products-produto-item',
  templateUrl: './produto-item.component.html',
  styles: [],
})
export class ProdutoItemComponent implements OnInit {
  @Input() produto: Produto;

  constructor() {
  }

  ngOnInit(): void {}
}
