import { Component, OnDestroy, OnInit } from '@angular/core';
import { Produto } from '../../models/produto';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProdutosService } from '../../services/produtos.service';

@Component({
  selector: 'products-produtos-lista',
  templateUrl: './produtos-lista.component.html',
  styles: [],
})
export class ProdutosListaComponent implements OnInit, OnDestroy {
  produtos: Produto[] = [];
  endSubs$: Subject<any> = new Subject();
  constructor(private produtosServices: ProdutosService) {}

  ngOnInit(): void {
    this._getProdutos();
  }
  ngOnDestroy(): void {
    this.endSubs$.next(void 0);
    this.endSubs$.complete();
  }

  private _getProdutos() {
    //TODO limitar quantidade de produtos do request
    this.produtosServices
      .getProdutos()
      .pipe(takeUntil(this.endSubs$))
      .subscribe((produtos) => {
        this.produtos = produtos;
      });
  }
}
