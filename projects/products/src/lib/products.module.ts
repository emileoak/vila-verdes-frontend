import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { ProdutoItemComponent } from './components/produto-item/produto-item.component';
import { ProdutosListaComponent } from './components/produtos-lista/produtos-lista.component';
import { ProductsComponent } from './products.component';

@NgModule({
  declarations: [
    ProductsComponent,
    ProdutoItemComponent,
    ProdutosListaComponent,
  ],
  imports: [CommonModule, ButtonModule],
  exports: [ProductsComponent, ProdutoItemComponent, ProdutosListaComponent],
})
export class ProductsModule {}
