import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'env/environment';
import { Planta } from '../models/planta';

@Injectable({
  providedIn: 'root',
})
export class PlantasService {
  apiUrlPlantas = environment.apiUrl + 'plantas';
  constructor(private http: HttpClient) {}

  getPlantas(): Observable<Planta[]> {
    return this.http.get<Planta[]>(this.apiUrlPlantas);
  }

  getPlanta(plantaId: number): Observable<Planta> {
    return this.http.get<Planta>(`${this.apiUrlPlantas}/${plantaId}`);
  }

  createPlanta(planta: Planta): Observable<Planta> {
    return this.http.post<Planta>(this.apiUrlPlantas, planta);
  }

  updatePlanta(planta: Planta): Observable<Planta> {
    return this.http.put<Planta>(`${this.apiUrlPlantas}/${planta.id}`, planta);
  }

  deletePlanta(plantaId: number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrlPlantas}/${plantaId}`);
  }
}
