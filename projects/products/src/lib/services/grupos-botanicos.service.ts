import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GrupoBotanico } from '../models/grupo-botanico';
import { Observable } from 'rxjs';
import { environment } from 'env/environment';

@Injectable({
  providedIn: 'root',
})
export class GruposBotanicosService {
  apiUrlGruposBotanicos = environment.apiUrl + 'grupos-botanicos';
  constructor(private http: HttpClient) {}

  getGruposBotanicos(): Observable<GrupoBotanico[]> {
    return this.http.get<GrupoBotanico[]>(this.apiUrlGruposBotanicos);
  }

  getGrupoBotanico(grupoBotanicoId: number): Observable<GrupoBotanico> {
    return this.http.get<GrupoBotanico>(
      `${this.apiUrlGruposBotanicos}/${grupoBotanicoId}`
    );
  }

  createGrupoBotanico(grupoBotanico: GrupoBotanico): Observable<GrupoBotanico> {
    return this.http.post<GrupoBotanico>(
      this.apiUrlGruposBotanicos,
      grupoBotanico
    );
  }

  updateGrupoBotanico(grupoBotanico: GrupoBotanico): Observable<GrupoBotanico> {
    return this.http.put<GrupoBotanico>(
      `${this.apiUrlGruposBotanicos}/${grupoBotanico.id}`,
      grupoBotanico
    );
  }

  deleteGrupoBotanico(grupoBotanicoId: number): Observable<any> {
    return this.http.delete<any>(
      `${this.apiUrlGruposBotanicos}/${grupoBotanicoId}`
    );
  }
}
