import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'env/environment';
import { Produto } from '../models/produto';

@Injectable({
  providedIn: 'root',
})
export class ProdutosService {
  apiUrlProdutos = environment.apiUrl + 'produtos';
  constructor(private http: HttpClient) {}

  getProdutos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.apiUrlProdutos);
  }

  getProduto(produtoId: number): Observable<Produto> {
    return this.http.get<Produto>(`${this.apiUrlProdutos}/${produtoId}`);
  }

  createProduto(produto: Produto): Observable<Produto> {
    return this.http.post<Produto>(this.apiUrlProdutos, produto);
  }

  updateProduto(produto: Produto): Observable<Produto> {
    return this.http.put<Produto>(
      `${this.apiUrlProdutos}/${produto.id}`,
      produto
    );
  }

  deleteProduto(produtoId: number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrlProdutos}/${produtoId}`);
  }
}
