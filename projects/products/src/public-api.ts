/*
 * Public API Surface of products
 */

export * from './lib/products.service';
export * from './lib/products.component';
export * from './lib/products.module';
export * from './lib/services/grupos-botanicos.service';
export * from './lib/services/plantas.service';
export * from './lib/services/produtos.service';
export * from './lib/models/grupo-botanico';
export * from './lib/models/planta';
export * from './lib/models/produto';
