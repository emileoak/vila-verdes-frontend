import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  goToAdmin() {
    window.open('http://dev.vilaverdes.com.br/admin/');
  }
}
